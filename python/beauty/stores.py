
import operator as ops
import pandas as pd


class ArrayGroup(pd.DataFrame):
    """
    Helper class representing an array of Series

    The class is indexable by the name of the provided Series.
    """

    _metadata = ['name']

    def __init__(self, *args, **kwargs):
        """
        Create a new group. See pandas.DataFrame for the constructor
        parameters.

        name -- an optional name for this group
        """
        self.name = kwargs.pop('name', None)
        super(ArrayGroup, self).__init__(*args, **kwargs)

    @property
    def _constructor(self):
        return ArrayGroup

    @property
    def _constructor_sliced(self):
        return Series

    @property
    def arrays(self):
        """Returns the list of Series in this array"""
        return [s for _, s in self.items()]

    @property
    def x(self):
        """Returns the common x-axis values for this array of Series"""
        return self.index

    @property
    def ys(self):
        """
        Returns the y-axis values for this array of Series.

        This is a numpy array organized with one row per Series.
        """
        return self.values.T


class Series(pd.Series):
    """Helper class to work with series of points in a 2D space"""

    def __init__(self, *args, **kwargs):
        """
        Create a new series. See pandas.Series for the constructor
        parameters.
        """
        super(Series, self).__init__(*args, **kwargs)

    @property
    def _constructor(self):
        return Series

    @property
    def _constructor_expanddim(self):
        return ArrayGroup

    @property
    def x(self):
        return self.index

    @property
    def y(self):
        return self.values

    def alignto(self, other):
        """
        Return a new Series x-aligned with the provided one

        The y values are linearly interpolated to match the provided x values.
        For x values outside the available range, nan is returned.
        """
        # 'reindex' will introduce NaN for the new indexes
        # the NaN will then be replaced by 'interpolate'
        # However if some NaN already exists in the data, we
        # do not want to interpolate.
        # The way to deal with this is to identify the indexes the original
        # NaN values
        nan_locations = self.index[self.isnull()]

        # Re-index will add NaN for new, non-existing indexes
        tmp = self.reindex(self.index.union(other.index))

        # Now we slice the re-indexed series at the location of the
        # original NaN and we build a mask excluding all consecutive
        # NaN values at the edges. NaN values within the slices will
        # be preserved and interpolated.
        # This also helps with another problem: interpolate
        # always extrapolates outside the range.
        # See https://github.com/pandas-dev/pandas/issues/8000

        nan_locations = nan_locations.insert(0, tmp.index[0])
        nan_locations = nan_locations.insert(len(nan_locations), tmp.index[-1])

        slices = ((tmp.loc[s:e].bfill().notnull() &
                   tmp.loc[s:e].ffill().notnull())
                  for s, e in zip(nan_locations[:-1],
                                  nan_locations[1:]))
        mask = pd.concat(slices, axis=1, copy=False).all(axis=1)
        tmp.interpolate(method='index', limit_direction='both', inplace=True)
        tmp.where(mask, inplace=True)
        return tmp.reindex(other.index)

    def correlate(self, other, reference=None):
        """
        Return a new Series representing the correlation
        of this (I) and the provided (K) series.

        I = f(x), K = g(x') --> I = f'(K)

        I values are linearly interpolated to match K's x' values.
        For x values outside the available range, nan is returned.

        Optionally a reference series can be provided. In this case, both I
        and K values are interpolated to match the reference's x values.
        """
        aligned_y = self.alignto(reference if reference is not None else other)
        aligned_x = other.alignto(reference)\
            if reference is not None else other
        s = Series(index=aligned_x.values, data=aligned_y.values)
        # In case a third reference is used, the x-axis (other's values
        # aligned to the reference) may now contain NaN. We should drop them
        s = s.loc[s.index.dropna()]
        s.sort_index(inplace=True)
        return s
