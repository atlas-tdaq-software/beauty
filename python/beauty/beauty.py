
import os
import operator as ops
import datetime as dt
import time
import pytz
import libpbeastpy
from . import stores


class Beauty(libpbeastpy.ServerProxy):
    """
    Helper class extending the pbeastpy interface
    """

    NOCOOKIE = libpbeastpy.CookieSetup.NoCookie
    AUTOUPDATEKERBEROS = libpbeastpy.CookieSetup.AutoUpdateKerberos

    @staticmethod
    def _localtime_beasttime(timezone):

        if timezone is None:
            timezone = 'UTC'

        zone = pytz.timezone(timezone)
        epoch = pytz.utc.localize(dt.datetime.utcfromtimestamp(0))

        def l2b(localtime):
            utctime = zone.localize(localtime).astimezone(pytz.utc)
            return int((utctime - epoch).total_seconds() * 1000000)

        def b2l(beasttime):
            utctime = dt.datetime.utcfromtimestamp(beasttime / 1000000.)
            utctime = pytz.utc.localize(utctime)
            return utctime.astimezone(zone)

        return l2b, b2l

    def __init__(self, server, timezone='Europe/Zurich',
                 cookie_setup=None):
        """
        Creates a Beauty instance.

        server -- the pBeast server
        timezone -- the timezone used to interpret the datetime objects
        cookie_setup -- if None, the default pBeast authentication will apply,
        possily controlled by the environment.
        NOCOOKIE will use not authentication.
        AUTOUPDATEKERBEROS will re-use an existing Kerberos cookie.
        """

        args = [server]

        if cookie_setup is not None:
            args.append(cookie_setup)

        super(Beauty, self).__init__(*args)

        self._beastconverter, self._localtimeconverter =\
            Beauty._localtime_beasttime(timezone)

    def timeseries(self, since, till, partition, isclass, attribute,
                   source='.*', array_index=None, regex=True,
                   downsample_interval=0, all_publications=False):
        """
        Query the pbeast data.

        Returns a list of Series or ArrayGroup depending on the data type.
        The Series or ArrayGroup names correspond to the data source.
        For ArrayGroup, the Series names correspond to the index in the array.

        The x-axis values are time coordinate in pbeast time representation
        (microseconds since the epoch), y-axis values are floats, if possible,
        or whatever other type if not convertible to float (e.g. strings).

        since -- datetime for the interval start
        till -- datetime for the interval end
        partition -- partition name
        isclass -- IS type
        attribute -- the IS attribute for which values will be retrieved
        source -- an optional source of the values. By the default all sources
        will be queried (e.g. all DCMs)
        array_index -- for array types, an optional index to be selected in
        the array
        regex -- specify is the source string should be interpreted are a
        regular expression
        downsample_interval -- optional downsampling interval in seconds
        all_publications -- retrieve data for all publications, even if the
        value is unchanged
        """

        since = self._beastconverter(since)
        till = self._beastconverter(till)

        result = self.get_data(partition, isclass, attribute,
                               source, regex, since, till,
                               downsample_interval, all_publications)

        if len(result) == 0:
            msg = ('No series found. Query:',
                   partition, isclass, attribute, source)
            msg = ' '.join(msg)
            raise ValueError(msg)
        elif len(result) > 1:
            msg = ('More than one data type in interval. Query:',
                   partition, isclass, attribute, source)
            msg = ' '.join(msg)
            raise ValueError(msg)

        result = result[0]

        dtype = float if result.type in [libpbeastpy.DataType.DOUBLE,
                                         libpbeastpy.DataType.FLOAT,
                                         libpbeastpy.DataType.S8,
                                         libpbeastpy.DataType.S16,
                                         libpbeastpy.DataType.S32,
                                         libpbeastpy.DataType.S64,
                                         libpbeastpy.DataType.U8,
                                         libpbeastpy.DataType.U16,
                                         libpbeastpy.DataType.U32,
                                         libpbeastpy.DataType.U64] else None

        series = []

        for series_name in result.data:

            timeseries = result.data[series_name]

            # Convert beast time into python Datetime
            timeseries = [(self.convert_beasttime(el.ts), el.value)
                          for el in timeseries]

            # Make sure the data are sorted by time
            timeseries.sort(key=ops.itemgetter(0))

            if result.is_array:
                i = map(ops.itemgetter(0), timeseries)
                data_series = zip(*map(ops.itemgetter(1),
                                       timeseries))

                data_series =\
                    dict((idx, d) for idx, d in enumerate(data_series)
                         if array_index is None or idx == array_index)

                ag = stores.ArrayGroup(name=series_name, data=data_series,
                                       index=i, dtype=dtype)

                series.append(ag)
            else:
                i, d = zip(*timeseries)
                series.append(stores.Series(data=d, index=i,
                                            name=series_name, dtype=dtype))

        series.sort(key=ops.attrgetter('name'))
        return series

    def convert_beasttime(self, t):
        """
        Converts beast time representation into datetime objects in the
        selected timezone.

        t -- a beast time value or a list of beast time values
        """
        try:
            return [self._localtimeconverter(e) for e in t]
        except TypeError:
            return self._localtimeconverter(t)
