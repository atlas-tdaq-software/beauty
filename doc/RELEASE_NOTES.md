# beauty

## nightly

### Remove automated setting of pBeast server

The initial `Beauty` implementation provided code automatically setting the pBeast server based on the values of environmental variables. This logic proved to be weak and not easily maintainable:
* especially in testbed, the pBeast server name has changed many times
* overloading the pBeast environmental variable for the authentication method for setting the server name is strictly speaking incorrect.

The new implementation removes completely this detection code. It is instead responsibility of the user to always provide a server name to the `Beauty` constructor.

**Old code relying on the previous implicit mechanism will fail:**

```python
>>> import beauty
>>> beauty.Beauty()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: __init__() missing 1 required positional argument: 'server'
```

### Control pBeast authentication method

Different pBeast servers require different authentication methods. Currently two methods are support:
* no authentication required
* authentication via an existing Kerberos token

The authentication method to be used can be controlled via a dedicated environmental variable (`PBEAST_SERVER_SSO_SETUP_TYPE`) or via the library API.
The latter method is now exposed in the `Beauty` interface. The `Beauty.__init__` method accepts a keyword argument `cookie_setup`. Valid values are:
* `None` &rarr; the default behaviour of the pBeast library will be used. The environment variable will be respected, if set
* `Beauty.NOCOOKIE` &rarr; no authentication required
* `Beauty.AUTOUPDATEKERBEROS` &rarr; authentication via Kerberos token

```python
>>> import beauty
>>> b = beauty.Beauty('http://someserver', cookie_setup=beauty.Beauty.NOCOOKIE)
```

## tdaq-09-04-00

Beauty is included in the TDAQ release. For reference the old changelog is reported below.

# Old Changelog

[Full Changelog](/../compare/beauty-02-00-00...master)

## Enhancements

## Fixes

## Internals

# beauty-02-00-00 (19/02/2021)

[Full Changelog](/../compare/beauty-01-02-00...beauty-02-00-00)

[Release Notes](/../tags/beauty-02-00-00)

## Enhancements

## Fixes

 - The **deprecated** method `Series.align` has been finally removed. `Series.alignto` must be used instead.

## Internals

# beauty-01-02-01 (24/11/2020)

[Full Changelog](/../compare/beauty-01-02-00...beauty-01-02-01)

## Enhancements

## Fixes

## Internals

  - LICENSE and NOTICE files added

# beauty-01-02-00 (22/09/2020)

[Full Changelog](/../compare/beauty-01-01-02...beauty-01-02-00)

## Enhancements

## Fixes

## Internals

  - Drop Python2 support

# beauty-01-01-02 (28/02/2020)

[Full Changelog](/../compare/beauty-01-01-00...beauty-01-01-02)

## Enhancements

## Fixes

  - The open GPN fallback server is not available anymore. If the server to be used cannot be autodetected and no explicit server is provided to the Beauty constructor an exception will be raised

## Internals

  - Fix units test execution
  - Improve Python2/Python3 compatibility

# beauty-01-01-00 (01/11/2019)

[Full Changelog](/../compare/beauty-01-00-01...beauty-01-01-00)

[Release Notes](/../tags/beauty-01-01-00)

## Enhancements

## Fixes

## Internals

  - Prevent Python bytecode generation during testing
  - Implement Python3 compatibility. This may cause higher memory usage in Python2

# beauty-01-00-01 (13/03/2019)

[Full Changelog](/../compare/beauty-01-00-00...beauty-01-00-01)

[Release Notes](/../tags/beauty-01-00-01)

## Enhancements

## Fixes

## Internals

 - The Python style test from cmake is not execute in the CI environment. Instead a dedicated container providing pycodestyle is used in this case.

# beauty-01-00-00 (08/03/2019)

[Full Changelog](/../compare/beauty-00-03-00...beauty-01-00-00)

[Release Notes](/../tags/beauty-01-00-00)

## Enhancements

 - beauty is now a proper python module

## Fixes

## Internals

 - Port to new pandas API for types
 - cmake file added in preparation for inclusion in the TDAQ release
 - scripts added for automated testing in TDAQ CI

# beauty-00-03-00 (07/03/2019)

[Full Changelog](/../compare/beauty-00-02-00...beauty-00-03-00)

[Release Notes](/../tags/beauty-00-03-00)

## Enhancements

  - The `Series.correlate` method now accetps a keyword argument `reference`. If provided, the correlation will be performed based on the timebase of the reference Series.

## Fixes

  - Fix interpolation method. By default pandas.Series.interpolate ignores the index and considers the values as equally spaced.

## Internals

  - Enable unit testing with Gilab CI

# beauty-00-02-00 (23/08/2018)

[Full Changelog](/../compare/beauty-00-01-03...beauty-00-02-00)

[Release Notes](/../tags/beauty-00-02-00)

## Enhancements

## Fixes

 - `Series.align` marked as **deprecated**. `Series.alignto` should be used instead. The rational for this is that the original method is involuntarily overwriting `pandas.Series.align`, which however has a completely different behaviour.

## Internals

# beauty-00-01-03 (15/08/2017)

[Full Changelog](/../compare/beauty-00-01-02...beauty-00-01-03)

[Release Notes](/../tags/beauty-00-01-03)

## Enhancements

   - Improve align performance using a more appropriate DataFrame constructor
   - Show in the sample notebook how to prevent missing cookie errors

## Fixes

   - Downsampled data were manipulated incorrectly, leading to crashes
   - Use new pBeast server in P1 (see [this Jira comment](https://its.cern.ch/jira/browse/ADAMATLAS-285?focusedCommentId=1578813&page=com.atlassian.jira.plugin.system.issuetabpanels:comment-tabpanel#comment-1578813))

## Internals

   - Add unit test for Downsampled data

# beauty-00-01-02 (20/04/2017)

[Full Changelog](/../compare/beauty-00-01-01...beauty-00-01-02)

[Release Notes](/../tags/beauty-00-01-02)

## Enhancements

   - By default beauty will only query pbeast only for changes in values, i.e. the timestamp of publications with constant values will not be retrieve. A new argument `all_publications` has been added to the `Beauty.timeseries` method that enables fetching all updates.

## Fixes

## Internals

   - Unit testing coverage expanded
   - Remove workaround for [ADAMATLAS-293](https://its.cern.ch/jira/browse/ADAMATLAS-293). The underlying problem was fixed in [ADHI-4396](https://its.cern.ch/jira/browse/ADHI-4396).

# beauty-00-01-01 (10/04/2017)

[Full Changelog](/../compare/beauty-00-01-00...beauty-00-01-01)

[Release Notes](/../tags/beauty-00-01-01)

## Enhancements

## Fixes

   - During alignment the NaN values of the series were not taken into special consideration and interpolated away

## Internals

   - Unit testing for stores added
   - Adapt to the libpbeastpy API (ADHI-4386)

# beauty-00-01-00 (30/03/2017)

[Full Changelog](/../compare/beauty-00-00-01...beauty-00-01-00)

[Release Notes](/../tags/beauty-00-01-00)

## Enhancements

   - Underlying data stores replaced with a panda-based implementations
