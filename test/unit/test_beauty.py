
import unittest
import os
import pytz
import datetime as dt
import numpy as np
import pandas as pd
import mock
import collections


class MockQueryData(object):

    def __init__(self, since=None,
                 till=None, type=None, is_array=None, data=None):
        self.since = since
        self.till = till
        self.type = type
        self.is_array = is_array
        self.data = data


MockDataPoint = collections.namedtuple('MockDataPoint', ['ts', 'value'])

MockDownsampledDataPoint =\
    collections.namedtuple('MockDownsampledDataPoint',
                           ['ts', 'value', 'min_value', 'max_value'])


class MockServerProxy(object):

    COOKIE_UNSET = "Unset"
    inspect = None
    mockdata = None
    downsampling = None
    allpoints = None

    def __init__(self, server, cookie_setup=COOKIE_UNSET):
        self._server = server
        self._cookie_setup = cookie_setup
        MockServerProxy.inspect = self

    def get_data(self, partition=None, isclass=None, variable=None,
                 source=None, regex=False, since=None, till=None,
                 downsampling=0, allpoints=False):

        MockServerProxy.downsampling = downsampling
        MockServerProxy.allpoints = allpoints

        for e in MockServerProxy.mockdata:
            e.since = since
            e.till = till

        return MockServerProxy.mockdata


module_mock = mock.MagicMock()
module_mock.ServerProxy = MockServerProxy
module_mock.DataType.DOUBLE = module_mock.DataType.FLOAT = float
module_mock.DataType.S8 = module_mock.DataType.S16 =\
    module_mock.DataType.S32 = module_mock.DataType.S64 =\
    module_mock.DataType.U8 = module_mock.DataType.U16 =\
    module_mock.DataType.U32 = module_mock.DataType.U64 = int

with mock.patch.dict('sys.modules', **{
        'libpbeastpy': module_mock, }):
    import beauty


class BeautyTest(unittest.TestCase):

    def test_construct(self):

        with self.assertRaises(TypeError):
            beauty.Beauty()

        server = 'myserver'
        bt = beauty.Beauty(server)
        self.assertEqual(MockServerProxy.inspect._server, server)
        self.assertEqual(MockServerProxy.inspect._cookie_setup,
                         MockServerProxy.COOKIE_UNSET)

        bt = beauty.Beauty(server, cookie_setup=None)
        self.assertEqual(MockServerProxy.inspect._cookie_setup,
                         MockServerProxy.COOKIE_UNSET)

        bt = beauty.Beauty(server,
                           cookie_setup=beauty.Beauty.NOCOOKIE)
        self.assertEqual(MockServerProxy.inspect._cookie_setup,
                         beauty.Beauty.NOCOOKIE)

        bt = beauty.Beauty(server,
                           cookie_setup=beauty.Beauty.AUTOUPDATEKERBEROS)
        self.assertEqual(MockServerProxy.inspect._cookie_setup,
                         beauty.Beauty.AUTOUPDATEKERBEROS)

    def test_timezone(self):

        bt = beauty.Beauty('dummyserver')

        epoch = pytz.utc.localize(dt.datetime.utcfromtimestamp(0))
        t = dt.datetime(2016, 7, 18, 10, 20, 0)
        t = pytz.utc.localize(t)
        beasttime = (t - epoch).total_seconds() * 1000000

        # The default timezone setting in Europe/Zurich
        converted = bt.convert_beasttime(beasttime)
        local = t.astimezone(pytz.timezone('Europe/Zurich'))
        self.assertEqual(local, converted)

        # Alternatively we can pass a different timezone
        zone = 'Brazil/West'
        bt = beauty.Beauty('dummyserver', timezone=zone)
        converted = bt.convert_beasttime(beasttime)
        local = t.astimezone(pytz.timezone(zone))
        self.assertEqual(local, converted)

    def test_get_data(self):

        bt = beauty.Beauty('dummyserver')

        since = dt.datetime(2016, 7, 18, 10, 20, 0)
        till = dt.datetime(2016, 7, 18, 10, 20, 0)

        d = {'series': [MockDataPoint(0, 10),
                        MockDataPoint(2000000, 20)]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=int, is_array=False, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')[0]

        self.assertEqual(MockServerProxy.downsampling, 0)
        self.assertEqual(MockServerProxy.allpoints, False)

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate',
                               downsample_interval=10,
                               all_publications=True)[0]

        self.assertEqual(MockServerProxy.downsampling, 10)
        self.assertEqual(MockServerProxy.allpoints, True)

    def test_fetch_series(self):

        bt = beauty.Beauty('dummyserver')

        since = dt.datetime(2016, 7, 18, 10, 20, 0)
        till = dt.datetime(2016, 7, 18, 10, 20, 0)

        with self.assertRaises(ValueError):
            MockServerProxy.mockdata = []
            bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')

        with self.assertRaises(ValueError):
            MockServerProxy.mockdata = [MockQueryData(), MockQueryData()]
            bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')

        d = {'series': [MockDataPoint(0, 10),
                        MockDataPoint(2000000, 20)]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=int, is_array=False, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')[0]
        self.assertEqual(type(result), beauty.stores.Series)
        self.assertEqual(result.name, 'series')
        self.assertEqual(result.dtype, float)
        self.assertTrue(all(result.y == [10, 20]))
        self.assertTrue(all(result.x.astype(np.int64) == [0, 2000000000]))

        d = {'series1': [MockDataPoint(0, 10),
                         MockDataPoint(2000000, 20)],
             'series2': [MockDataPoint(0, 10),
                         MockDataPoint(2000000, 20)]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=int, is_array=False, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')
        self.assertEqual(len(result), 2)

        d = {'series': [MockDataPoint(0, 'string'),
                        MockDataPoint(2000000, 'string')]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=None, is_array=False, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')[0]
        self.assertTrue(pd.api.types.is_string_dtype(result.dtype))

        d = {'series': [MockDataPoint(0, dt.datetime(1970, 1, 1)),
                        MockDataPoint(2000000, dt.datetime(1970, 1, 2))]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=None, is_array=False, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')[0]
        self.assertTrue(pd.api.types.is_datetime64_dtype(result.dtype))

    def test_fetch_array(self):

        bt = beauty.Beauty('dummyserver')

        since = dt.datetime(2016, 7, 18, 10, 20, 0)
        till = dt.datetime(2016, 7, 18, 10, 20, 0)

        d = {'series': [MockDataPoint(0, [0, 10]),
                        MockDataPoint(2000000, [2, 20])]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=int, is_array=True, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')[0]
        self.assertEqual(type(result), beauty.stores.ArrayGroup)
        self.assertEqual(result.name, 'series')
        self.assertEqual(result[0].dtype, float)
        self.assertTrue(all(result.x.astype(np.int64) == [0, 2000000000]))
        self.assertTrue(np.all(result.ys == [[0, 2], [10, 20]]))

    def test_fetch_downsampled(self):

        bt = beauty.Beauty('dummyserver')

        since = dt.datetime(2016, 7, 18, 10, 20, 0)
        till = dt.datetime(2016, 7, 18, 10, 20, 0)

        d = {'series': [MockDownsampledDataPoint(0, 10, 5, 15),
                        MockDownsampledDataPoint(2000000, 20, 15, 25)]}

        MockServerProxy.mockdata =\
            [MockQueryData(since=None,
                           till=None, type=int, is_array=False, data=d), ]

        result = bt.timeseries(since, till, 'ATLAS', 'HLTSV', 'Rate')[0]
        self.assertEqual(type(result), beauty.stores.Series)
        self.assertEqual(result.name, 'series')
        self.assertEqual(result.dtype, float)
        self.assertTrue(all(result.y == [10, 20]))
        self.assertTrue(all(result.x.astype(np.int64) == [0, 2000000000]))
