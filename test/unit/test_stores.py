
import unittest
import random
import string
import warnings
import numpy as np
import datetime as dt

import beauty.stores as st


def random_string(size=10):
    return ''.join(random.choice(string.ascii_uppercase +
                                 string.ascii_lowercase +
                                 string.digits)
                   for _ in range(size))


class ArrayGroupTest(unittest.TestCase):

    def test_create(self):

        data1 = np.array(range(10))
        data2 = data1 * 2
        index = data1 * 10

        ag = st.ArrayGroup({0: data1, 1: data2}, index=index)

        self.assertEqual(ag.name, None)
        self.assertTrue(all(ag.x == index))
        self.assertTrue(all(ag[0].y == data1))
        self.assertTrue(all(ag[1].y == data2))

        self.assertEqual(len(ag.arrays), 2)
        self.assertEqual(type(ag.arrays[0]), st.Series)

        self.assertTrue(all(ag.ys[0] == data1))

        name = random_string()
        ag = st.ArrayGroup({0: data1, 1: data2}, index=index, name=name)

        self.assertEqual(ag.name, name)


class SeriesTest(unittest.TestCase):

    def test_create(self):

        data = range(10)
        index = [e * 2 for e in data]
        s = st.Series(data, index)

        self.assertTrue(all(s.x == index))
        self.assertTrue(all(s.y == data))
        self.assertEqual(s.name, None)

        s = st.Series(data, index, dtype=float)

        self.assertEqual(s.dtype, float)
        self.assertEqual(s.y.dtype, float)
        self.assertEqual(s.x.dtype, int)

        name = random_string()

        s = st.Series(data, index, name=name)

        self.assertEqual(s.name, name)

    def test_interaction(self):

        data = range(10)
        index = [e * 2 for e in data]
        s = st.Series(data, index, dtype=float)

        square = s * s

        self.assertEqual(type(square), st.Series)
        self.assertTrue(all(square.x == s.x))
        self.assertTrue(all(square.y == (s.y * s.y)))

    def aligntests(self, alignmethod):

        index1 = range(4)
        data1 = [10 * e for e in index1]
        s1 = st.Series(data=data1, index=index1)

        index2 = [-0.7] + [e + 0.7 for e in index1]
        data2 = index2
        s2 = st.Series(data=data2, index=index2)

        salign = alignmethod(s1, s2)

        self.assertTrue(all(salign.x == s2.x))
        self.assertTrue(np.isnan(salign.y[0]))
        self.assertTrue(np.isnan(salign.y[-1]))

        self.assertTrue(all(salign.y[1:-1] == [7, 17, 27]))

        # Check that NaN in the input series is preserved
        # and not interpolated away
        data1 = [10, np.nan, 30, 40]
        s1 = st.Series(data=data1, index=index1)

        salign = alignmethod(s1, s2)
        self.assertTrue(np.isnan(salign[0.7]))
        self.assertTrue(np.isnan(salign[1.7]))
        self.assertEqual(salign[2.7], 37)

        data1 = [np.nan, np.nan, 30, 40]
        s1 = st.Series(data=data1, index=index1)

        salign = alignmethod(s1, s2)
        self.assertTrue(np.isnan(salign[0.7]))
        self.assertTrue(np.isnan(salign[1.7]))

    def test_alignto(self):

        self.aligntests(st.Series.alignto)

    def test_correlate(self):

        index1 = range(4)
        data1 = [10 * e for e in index1]
        s1 = st.Series(data=data1, index=index1)

        index2 = [-0.6] + [e + 0.6 for e in index1]
        data2 = [3 * e for e in index2]
        s2 = st.Series(data=data2, index=index2)

        scorr = s1.correlate(s2)

        self.assertTrue(len(scorr) == len(s2))
        self.assertTrue(all(scorr.x == s2.y))
        self.assertTrue(np.isnan(scorr.y[0]))
        self.assertTrue(np.isnan(scorr.y[-1]))

        self.assertTrue(all(scorr.y[1:-1] == [6, 16, 26]))

        scorr = s1.correlate(s2, reference=s1)
        self.assertFalse(np.isnan(scorr.y).any())
        self.assertTrue(len(scorr) == len(s1))

        index_ref = [-0.9] + [-0.3] + [e + 0.9 for e in index1]
        value_ref = [3 * e for e in index_ref]
        ref = st.Series(data=value_ref, index=index_ref)

        scorr = s1.correlate(s2, reference=ref)
        self.assertFalse(np.isnan(scorr.x).any())
        self.assertTrue(scorr[2.7], 9)
        self.assertTrue(np.isnan(scorr.y[0]))

    def test_datetime(self):

        index1 = [dt.datetime(2017, 1, 1, 10, i) for i in range(10)]
        data1 = [9 * i for i in range(len(index1))]
        s1 = st.Series(data=data1, index=index1)

        timedelta = dt.timedelta(seconds=20)

        index2 = [t + timedelta for t in index1]
        s2 = st.Series(data=data1, index=index2)

        salign = s1.alignto(s2)

        self.assertTrue(all(salign.x == s2.x))
        self.assertEqual(salign.y[0], 3)
        self.assertTrue(np.isnan(salign.y[-1]))

        data1 = [9 * i for i in range(len(index1))]
        data1[2] = np.nan
        s1 = st.Series(data=data1, index=index1)

        salign = s1.alignto(s2)

        self.assertEqual(salign.y[0], 3)
        self.assertTrue(np.isnan(salign.y[1]))
        self.assertTrue(np.isnan(salign.y[2]))
        self.assertEqual(salign.y[3], 30)
        self.assertTrue(np.isnan(salign.y[-1]))

    def test_strings(self):

        index = [dt.datetime(2017, 1, 1, 10, i) for i in range(10)]

        data = [random_string(int(random.uniform(1, 100))) for _ in index]

        s = st.Series(data=data, index=index)

        self.assertTrue(all(s.y == data))
