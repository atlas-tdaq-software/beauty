#!/bin/bash

# This only works in the nightly builds and uses a service account normally
# used to copy the tar files and RPMs to /eos.
if [ -n "${EOS_ACCOUNT_USERNAME}" ]; then
  dir=$(mktemp)
  export KRB5CCNAME=FILE:${dir}
  trap "rm -f ${dir}" EXIT
  echo "${EOS_ACCOUNT_PASSWORD}" | /usr/bin/kinit -f ${EOS_ACCOUNT_USERNAME}@CERN.CH > /dev/null
elif [ -n "${CI}" ]; then
  exit 0
fi

eval $(get_lcg_env --prepend --python --ld numpy)
tdaq_python -m unittest discover -s test/unit
exit $?
