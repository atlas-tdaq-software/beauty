# Beauty

Extension of the libpbeastpy interface. It provides a (hopefully) simple programmatic way to query and manipulate values stored in pBeast. See also [this example notebook](test/ipynb/beauty.ipynb).
